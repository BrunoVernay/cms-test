import os

from oscrypto import asymmetric
from certbuilder import CertificateBuilder, pem_armor_certificate
from asn1crypto import pem, x509

from ConfigCl import Certif


def create_pki(ca_cert: Certif, signer_cert: Certif):
    if not os.path.exists(ca_cert.dir):
        os.mkdir(ca_cert.dir)
    # Generate and save the key and certificate for the root CA
    root_ca_public_key, root_ca_private_key = asymmetric.generate_pair('rsa', bit_size=ca_cert.key_size)

    with open(ca_cert.file + '.key', 'wb') as f:
        f.write(asymmetric.dump_private_key(root_ca_private_key, 'password'))

    builder = CertificateBuilder(
        ca_cert.cname,
        root_ca_public_key
    )
    builder.self_signed = True
    builder.ca = True
    root_ca_certificate = builder.build(root_ca_private_key)

    with open(ca_cert.file + '.crt', 'wb') as f:
        f.write(pem_armor_certificate(root_ca_certificate))

    # Generate an end-entity key and certificate, signed by the root
    end_entity_public_key, end_entity_private_key = asymmetric.generate_pair('rsa', bit_size=signer_cert.key_size)

    with open(signer_cert.file + '.key', 'wb') as f:
        f.write(asymmetric.dump_private_key(end_entity_private_key, None))

    builder = CertificateBuilder(
        signer_cert.cname,
        end_entity_public_key
    )
    builder.issuer = root_ca_certificate
    end_entity_certificate = builder.build(root_ca_private_key)

    with open(signer_cert.file + '.crt', 'wb') as f:
        f.write(pem_armor_certificate(end_entity_certificate))


def create_device_cert(device_cert: Certif):

    # Generate a device "RAUC encryption" key and certificate, self-signed
    device_public_key, device_private_key = asymmetric.generate_pair('ec', bit_size=160, curve='secp256r1')

    with open(device_cert.file + '.key', 'wb') as f:
        f.write(asymmetric.dump_private_key(device_private_key, None))

    builder = CertificateBuilder(
        device_cert.cname,
        device_public_key
    )
    builder.self_signed = True
    builder.ca = False
    device_certificate = builder.build(device_private_key)

    with open(device_cert.file + '.crt', 'wb') as f:
        f.write(pem_armor_certificate(device_certificate))


def check_pki(signer_cert: Certif):
    # https://github.com/wbond/asn1crypto/blob/master/docs/pem.md
    with open(signer_cert.file + '.crt', 'rb') as f:
        der_bytes = f.read()
        if pem.detect(der_bytes):
            type_name, headers, der_bytes = pem.unarmor(der_bytes)

    cert: x509.Certificate = x509.Certificate.load(der_bytes)
    print("\nSigner certif: ")
    print("  Issuer:\t", cert.issuer.human_friendly)
    print("  Subject:\t", cert.subject.human_friendly)

