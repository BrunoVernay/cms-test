import os

''' Encrypted config instance
Ideally the config should be loaded from external files, or inherit from a default config ...
'''


class Certif:
    dir = 'cert'

    def __init__(self, file, cname, key_size):
        self.file = os.path.join(self.dir, file)
        self.cname = cname
        self.key_size = key_size


class Cms:
    dir = 'cms_e'
    file = os.path.join(dir, 'bndl.cms')  # RAUC bundle signature
    plantuml = os.path.join(dir, 'cms.puml')
    plantjson = os.path.join(dir, 'cms-json.puml')
    plantjsonflat = os.path.join(dir, 'cms-flat.puml')


class Rauc:
    bundle = 'tobebundle'  # Path to the files to be bundled by rauc
    out = 'raucout'  # RAUC outputs
    out_bndl = os.path.join(out, 'bndl.raucb')  # RAUC outputs
    manifest = '''[update]
compatible=rauc-demo-RiscV
version=2022.08-2
[bundle]
format=crypt
[image.rootfs]
filename=rootfs.ext4.img
'''
    system = '''[system]
compatible=rauc-demo-RiscV
bootloader=uboot
[keyring]
path=../''' + Certif.dir + '''/root_ca.crt
check-purpose=any
'''


class Config:
    pki_ca = Certif('root_ca',  # Careful, it is defined in Config.rauc.system too ...
                    {
                        'country_name': 'FR',
                        'locality_name': 'Paris',
                        'organization_name': 'Test',
                        'common_name': 'Test Root CA 1',
                    },
                    2048)
    pki_signer = Certif('rauc-sign',
                        {
                            'country_name': 'FR',
                            'locality_name': 'Grenoble',
                            'organization_name': 'Test',
                            'common_name': 'RAUC 2 bundle signer',
                        },
                        1024)
    pki_device = Certif('rauc-encrypt',
                        {
                            'country_name': 'IN',
                            'locality_name': 'Jabil',
                            'organization_name': 'Device X',
                            'common_name': 'Device SN One',
                        },
                        1024)
    rauc = Rauc
    cms = Cms
