import os


class Certif:
    def __init__(self, file, cname, key_size):
        self.file = file
        self.cname = cname
        self.key_size = key_size


class Cms:
    dir = 'cms'
    file = os.path.join(dir, 'bndl.cms')      # RAUC bundle signature
    plantuml = os.path.join(dir, 'cms.puml')
    plantjson = os.path.join(dir, 'cms-json.puml')
    plantjsonflat = os.path.join(dir, 'cms-flat.puml')


class Rauc:
    bundle = 'tobebundle'   # Path to the files to be bundled by rauc
    out = 'raucout'     # RAUC outputs
    out_bndl = os.path.join(out, 'bndl.raucb')      # RAUC outputs
    manifest = '''[update]
compatible=rauc-demo-RiscV
version=2022.08-2

[bundle]
format=verity

[image.rootfs]
filename=rootfs.ext4.img
'''
    system = '''[system]
compatible=rauc-demo-RiscV
bootloader=uboot
data-directory=/srv/rauc
bundle-formats=-plain

[keyring]

path=../out/root_ca.crt
#check-purpose=codesign
check-purpose=any

[slot.rootfs.0]
device=/dev/sda0
type=ext4
bootname=system0
'''


class Config:
    pki_ca = Certif('out/root_ca',      # Careful, it is defined in Config.rauc.system too ...
                    {
                        'country_name': 'FR',
                        'locality_name': 'Paris',
                        'organization_name': 'Test',
                        'common_name': 'Test Root CA 1',
                    },
                    3072)
    pki_signer = Certif('out/rauc-sign',
                        {
                            'country_name': 'FR',
                            'locality_name': 'Grenoble',
                            'organization_name': 'Test',
                            'common_name': 'RAUC bundle signer',
                        },
                        2048)
    rauc = Rauc
    cms = Cms
