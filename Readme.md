# RAUC CMS structure

Investigate more than test:
- Create a CMS with RAUC
- Generate diagrams based on the CMS

> Note:  It will only generate files on Linux. (Check prerequisite). But analysis can then be done on Windows.

It will generate a plantUML and a plantJSON diagram that should be easy to read ...

Diagram examples:
![json CMS](doc/cms2-json.svg)
![json flat](doc/cms-flat.svg)
![PlantUML CMS](doc/cms.svg)


## Prerequisite

- Python
  - Create Virtual Environment `python -m venv env`
  - Use Virtual Environment : `source env/bin/activate`
  - Install dependencies `pip install -r requirements.txt`
- Rauc must be installed
  - exec must be available (for example `ln -s ../somewhere/rauc/rauc .`) 
     and must match with path defined in `rauc.py`
 

## Run

- Use Virtual Environment : `source env/bin/activate` (on Linux)


## RAUC

RAUC has dependencies: `dnf install squashfs-tools`

`./configure --disable-service --disable-network`
` rauc 1.7.119-3da9c`

With encryption, can do further tests. 
ex: `./rauc --no-verify  --key=cert/rauc-encrypt.key -d info raucout/bndl.raucb_e`

## TODO

See if https://docs.pytest.org/en/stable/explanation/fixtures.html could be used to run multiple config.

Create the `system.conf` file and put it as parameter in rauc commands. 
(by default it uses `/etc/rauc/system.conf`)

Add arguments to 
 - only check the CMS once it has been created
 - Clear all generated files
 - Output unfiltered diagrams (harder to read, but useful) 


## Development

The project is divided in modules:
- PKI to create the key and certificates
- RAUC to launch rauc commands, create bundles
- CMS is about the Cryptographic Message Syntax ...
- The configuration should allow to generate different test/investigations without modifying other modules


### Crypto libraries

I am using [asn1crypto](https://github.com/wbond/asn1crypto/) (and the related modules 
like [certbuilder](https://github.com/wbond/certbuilder)). 

But then I saw most examples are using https://cryptography.io/ ...

I should probably converge on **one** library and https://cryptography.io/ seems to be the better supported project.
But that would be a lot of work ...

(and there is also https://www.pycryptodome.org/)


## References
- About verifying signatures ... https://github.com/wbond/asn1crypto/issues/89 
- https://www.rfc-editor.org/rfc/rfc5652.html 
