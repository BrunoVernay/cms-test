import os
import subprocess

import ConfigCl

exec_path = './rauc'    # Path to RAUC executable
system_conf = 'system.conf'


def create_bundle(config: ConfigCl):
    if os.path.exists(config.rauc.out_bndl):
        print('Bundle already exists. Deleting ...')
        os.remove(config.rauc.out_bndl)
    args = [exec_path,
            '--cert', config.pki_signer.file + '.crt',
            '--key', config.pki_signer.file + '.key',
            'bundle', config.rauc.bundle,
            config.rauc.out_bndl,
            ]
    return subprocess.run(' '.join(args), shell=True, check=True)


def encrypt_bundle(config: ConfigCl):
    if os.path.exists(config.rauc.out_bndl+'_e'):
        print('Encrypted bundle already exists. Deleting ...')
        os.remove(config.rauc.out_bndl+'_e')
    args = [exec_path,
            '--to=' + config.pki_device.file + '.crt',    # Check certificate authenticity (in the real world)
            '--conf', os.path.join(config.rauc.out, system_conf),
            '-d',
            'encrypt',
            config.rauc.out_bndl, config.rauc.out_bndl+'_e',
            ]
    return subprocess.run(' '.join(args), shell=True, check=True)


def extract_signature(config: ConfigCl):
    if os.path.exists(config.cms.file):
        print('CMS already exists. Deleting ...')
        os.remove(config.cms.file)
    args = [exec_path,
            '--conf', os.path.join(config.rauc.out, system_conf),
            'extract-signature', config.rauc.out_bndl,
            config.cms.file
            ]
    return subprocess.run(' '.join(args), shell=True, check=True)


def init(config: ConfigCl):
    if not os.path.exists(config.rauc.bundle):
        os.mkdir(config.rauc.bundle)
    if not os.path.exists(config.rauc.out):
        os.mkdir(config.rauc.out)
    if not os.path.exists(config.cms.dir):
        os.mkdir(config.cms.dir)

    manifest_path = os.path.join(config.rauc.bundle, 'manifest.raucm')
    if os.path.exists(manifest_path):
        print('Bundle manifest already exists. Deleting ...')
        os.remove(manifest_path)
    with open(manifest_path, 'w+') as fp:
        fp.writelines(config.rauc.manifest)

    image_path = os.path.join(config.rauc.bundle, 'rootfs.ext4.img')
    if os.path.exists(image_path):
        print('Bundle image already exists')
    else:
        with open(image_path, 'wb') as fp:
            fp.write(os.urandom(4096))  # Squashfs minimum size

    system_path = os.path.join(config.rauc.out, system_conf)
    if os.path.exists(system_path):
        print('System config already exists. Deleting ...')
        os.remove(system_path)
    with open(system_path, 'w+') as fp:
        fp.writelines(config.rauc.system)
