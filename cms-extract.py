# Utility to extract CMS from RAUC bundle

import sys


def usage():
    print('''  Output the CMS part of a RAUC bundle
    Usage: python cms-extract.py <bndl.raucb> | openssl cms -cmsout -print -inform DER ''', file=sys.stderr)


BYTES_OF_CMS_SIZE = 8  # CMS size is writen as 8 bytes at the end of the file
START_FROM_END = 2

if len(sys.argv) != 2:
    usage()
    exit(1)

file_name = sys.argv[1]

with open(file_name, "rb") as f:
    f.seek(-BYTES_OF_CMS_SIZE, START_FROM_END)
    raw = f.read(BYTES_OF_CMS_SIZE)
    cms_size = int.from_bytes(raw, byteorder='big')
    print('CMS Size: ', cms_size, file=sys.stderr)

    f.seek(-BYTES_OF_CMS_SIZE - cms_size, START_FROM_END)
    sys.stdout.buffer.write(f.read(cms_size))
