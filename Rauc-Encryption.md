# Investigate RAUC encryption 

## RAUC bundle structure

The RAUC bundle concatenate the firmware, the CMS and the size of the CMS on 8 bytes. 
You can use the utility script `cms-extract.py` to output the CMS and do the ASN.1 parsing:
`python cms-extract.py <bndl.raucb> | openssl cms -cmsout -print -inform DER`


## Asymmetric encryption

Check
 - `encrypt_bundle()` in `include/bundle.h` and `src/bundle.c`
 - `cms_encrypt()` and `cms_decrypt()` in `include/signature.h` and `src/signature.c` (should be named "cms" instead of "signature") 
   - It includes many `openssl/...h` ...


## Symmetric encryption
 
Code related to symmetric encryption of the bundle. (The one that does not change.)

This part is not relevant to this investigation

 - `encrypt_bundle_payload()` and `decrypt_bundle_payload()` in `src/bundle.c` 
 - `prepare_crypt()` in `src/bundle.c`
 - `include/crypt.h` 
 - `include/dm.h` is for Device Mapper

## References

- https://www.pengutronix.de/en/blog/2022-03-31-tutorial-rauc-bundle-encryption-using-meta-rauc.html very good explanation.

CMS: 
- Nice diagram https://docs.microsoft.com/en-us/windows/win32/seccrypto/encoding-enveloped-data 
