# Analyse the extracted CMS signature from RAUC bundle
import collections
import datetime
import hashlib
import json
import pprint

import asn1crypto
from asn1crypto import cms, keys
from oscrypto import asymmetric

import ConfigCl


class BytesEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, bytes):
            # return obj.decode('utf-8')
            return bytes(obj)[:7].hex() + '...'
        if isinstance(obj, datetime.date):
            return str(obj)[:-6]
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


class CmsAnalysis:
    json_comment: str = '<color:green>'
    msg_digests = {}

    def __init__(self, config: ConfigCl):
        self.config = config
        with open(self.config.cms.file, 'rb') as f:
            info = cms.ContentInfo.load(f.read())
        print("\nCMS structure: ")
        pp = pprint.PrettyPrinter(width=120, compact=False, depth=4)
        pp.pprint(info.native)
        assert 'signed_data' == info['content_type'].native, 'CMS should contain SignedData!'
        sd: cms.SignedData = info['content'].native
        print("\n Signed Data: ")
        pp.pprint(sd)
        eci: cms.EncapsulatedContentInfo = sd['encap_content_info']
        print('\nEncapsulated Content (AKA Message): ------------------------------------ BEGIN -')
        print(eci['content'].decode("utf-8"))
        print('Encapsulated Content (AKA Message): ------------------------------------ END ---')
        print('Message Digest(s):')
        for alg in sd['digest_algorithms']:
            h = alg['algorithm']
            hs = hashlib.new(h)
            hs.update(eci['content'])
            self.msg_digests[h] = hs.hexdigest()
            print('\t' + h + '(msg) ' + hs.hexdigest())
        print(
            '\nSince Signed Attributes are used, they include the message digest and they are the basis of the signature.')
        print('\n Signed Attributes digest:')
        for si in info['content']['signer_infos']:
            # https://github.com/panzi/verify-ehc/blob/main/verify_ehc.py#L1064
            # https://github.com/ly4k/Certipy/blob/main/certipy/lib/certificate.py#L459
            sa = si['signed_attrs']
            signed_attrs_bytes = bytearray(sa.dump())
            print(signed_attrs_bytes.hex())
            signed_attrs_bytes[0] = 0x11 | 0x20     #  ASN1_SET | ASN1_CONSTRUCTED
            print(signed_attrs_bytes.hex())
            pubkey_info: asn1crypto.keys.PublicKeyInfo = info['content']['certificates'][0].chosen.public_key
            # verify() will raise an InvalidSignature exception if the signature isn’t valid.
            asymmetric.rsa_pkcs1v15_verify(
                            asymmetric.load_public_key(pubkey_info.dump()),
                            si['signature'].native,
                            bytes(signed_attrs_bytes),
                            si['digest_algorithm']['algorithm'].native)
            print('Signature verif: OK')
        print('\n')

    def generate_plantuml(self):
        with open(self.config.cms.file, 'rb') as f:
            info = cms.ContentInfo.load(f.read())
        with open(self.config.cms.plantuml, 'w', encoding='utf-8') as f:
            f.write('@startuml')
            assert 'signed_data' == info['content_type'].native, 'CMS should contain SignedData!'
            f.write('\nmap SignedData {')
            sd: cms.SignedData = info['content'].native
            f.write('\nCMSVersion => ' + sd['version'])
            da = cms.DigestAlgorithms(sd['digest_algorithms'])
            if len(da.children) > 1:
                print('Case when there are more than one DigestAlgorithms is not managed yet!!')
                exit(1)
            else:
                f.write('\ndigestAlgorithms => ' + da.native[0]['algorithm'])
            f.write('\nEncapsulatedContentInfo => Data')
            f.write('\nCertificates => ')
            f.write('\nSignerInfos => ')
            f.write('\n}')
            eci = cms.EncapsulatedContentInfo(sd['encap_content_info'])
            f.write('\nObject Data\nData : ')
            f.write(str(eci['content'].native))
            f.write('\nSignedData::EncapsulatedContentInfo -> Data')

            f.write('\nmap "Certificate [0]" as c0 {')
            for cert in sd['certificates']:
                f.write('\ncert => "' + cert['tbs_certificate']['subject']['common_name'] + '"')
                f.write('\nalgo => ' + cert['signature_algorithm']['algorithm'])
                if None != cert['signature_algorithm']['parameters']:
                    f.write(" (" + str(cert['signature_algorithm']['parameters']) + ")")
                f.write('\nsignature => ' + bytes(cert['signature_value'])[:7].hex() + '...')
            f.write('\n}')
            f.write('\nSignedData::Certificates -> c0')

            f.write('\nmap "Signer Info [0]" as s0 {')
            for si in sd['signer_infos']:
                f.write('\nCMS version => ' + si['version'])
                f.write('\nSigner Identifier | Issuer => "' + si['sid']['issuer']['common_name'] + '"')
                f.write('\nSigner Identifier | Serial => ' + str(si['sid']['serial_number'])[:7] + '...')
                f.write('\nDigest Algo => ' + si['digest_algorithm']['algorithm'])
                if None != si['digest_algorithm']['parameters']:
                    f.write(" (" + str(si['digest_algorithm']['parameters']) + ")")
                f.write('\nsignature Algorithm  => ' + si['signature_algorithm']['algorithm'])
                if None != si['signature_algorithm']['parameters']:
                    f.write(" (" + str(si['signature_algorithm']['parameters']) + ")")
                f.write('\nsignature => ' + bytes(si['signature'])[:7].hex() + '...')
                for sa in si['signed_attrs']:
                    if 'message_digest' == sa['type']:
                        f.write('\nSigned Attr | msg digest => ')
                        f.write(bytes(sa['values'][0])[:7].hex() + '...')
                    elif 'signing_time' == sa['type']:
                        f.write('\nSigned Attr | ' + sa['type'] + ' => ')
                        f.write(str(sa['values'][0])[:-6])
                    else:
                        f.write('\nSigned Attr | ' + sa['type'] + ' => ')
                        f.write(str(sa['values'][0]))
            if si['unsigned_attrs'] is None:
                f.write('\nUnsigned Attr => ∅')
            else:
                for sa in si['unsigned_attrs']:
                    if 'message_digest' == sa['type']:
                        f.write('\nUnsigned Attr | msg digest => ')
                        f.write(bytes(sa['values'][0])[:7].hex() + '...')
                    elif 'signing_time' == sa['type']:
                        f.write('\nUnsigned Attr | ' + sa['type'] + ' => ')
                        f.write(str(sa['values'][0])[:-6])
                    else:
                        f.write('\nUnsigned Attr | ' + sa['type'] + ' => ')
                        f.write(str(sa['values'][0]))
            f.write('\n}')
            f.write('\nSignedData::SignerInfos -> s0')

            f.write('\n@enduml')

    def __my_filter(self, k, v):
        if k in ['digest_algorithms']:
            s = v[0]['algorithm']
            v.pop()
            return s
        if k in ['digest_algorithm', 'signature_algorithm', 'signature', 'algorithm'] and isinstance(v, dict):
            if 'algorithm' in v.keys():
                return v['algorithm']
            else:
                return v
        if k in ['issuer', 'subject']:
            return v['common_name']
        if k in ['encap_content_info']:
            s = {v['content_type']: str(v['content'])}
            for h, d in self.msg_digests.items():
                s[self.json_comment + h + '(' + v['content_type'] + ')'] = self.json_comment + d[:7 * 2] + '...'
            return s
        if k in ['signed_attrs']:
            assert len(v[0]['values']) == 1, 'Missing multiple values !!! '
            return {att['type']: att['values'][0] for att in v}
        if k == 'signer_infos':
            for si in v:
                if si['version'] == 'v1':  # See RFC
                    si['version'] = '1 ' + self.json_comment + '// Identifier is Issuer + SN'
                elif si['version'] == 'v3':  # See RFC
                    si['version'] = '3 ' + self.json_comment + '// Identifier is subjectKeyIdentifier'
                else:
                    si['version'] += ' ' + self.json_comment + '// Unknown version!'
        if k in ['public_key', 'extensions', 'validity', 'subject_public_key_info']:
            return '...'
        if k == 'content':
            return v.decode("utf-8")
        return v

    def __map_nested_dicts_modify(self, ob, f):
        if isinstance(ob, collections.abc.Mapping):
            for k, v in ob.items():
                ob[k] = f(k, v)
                if isinstance(v, collections.abc.Mapping):
                    self.__map_nested_dicts_modify(v, f)
                elif isinstance(v, list):
                    if len(v) == 1:
                        ob[k] = v[0]
                    self.__map_nested_dicts_modify(v, f)
        elif isinstance(ob, list):
            for v in ob:
                self.__map_nested_dicts_modify(v, f)

    def generate_json(self):
        with open(self.config.cms.file, 'rb') as f:
            info = cms.ContentInfo.load(f.read())
            assert 'signed_data' == info['content_type'].native, 'CMS should contain SignedData!'
            sd: cms.SignedData = info['content'].native
            self.__map_nested_dicts_modify(sd, self.__my_filter)
            content = json.dumps(sd, skipkeys=True, cls=BytesEncoder, indent=2)
        with open(self.config.cms.plantjson, 'w', encoding='utf-8') as f:
            f.write('@startjson\n')
            f.write(content)
            f.write('\n@endjson')
        with open(self.config.cms.plantjsonflat, 'w', encoding='utf-8') as f:
            f.write('@startuml\n')
            f.write('\njson "Signed Data" as sd ')
            f.write(content)
            f.write('\njson Abbrev {\n"sid": "Signer Identifier"\n}')
            f.write('\n@enduml')
