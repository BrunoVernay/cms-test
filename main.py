import argparse
import platform

import ConfigCl
import ConfigCl_2
import ConfigCl_e
import pki
import rauc
from cms import CmsAnalysis

# config = ConfigCl.Config
# config = ConfigCl_2.Config
config = ConfigCl_e.Config


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Investigate RAUC CMS structure. (generation requires Linux)')
    parser.add_argument('--clean', help='Remove the generated files')

    args = parser.parse_args()
    if 'Linux' == platform.system():
        pki.create_pki(config.pki_ca, config.pki_signer)
        pki.check_pki(config.pki_signer)
        pki.create_device_cert(config.pki_device)
        rauc.init(config)
        rauc.create_bundle(config)
        rauc.encrypt_bundle(config)
        rauc.extract_signature(config)
    else:
        print('No Linux: skipping RAUC steps.')
    anlz = CmsAnalysis(config)
    anlz.generate_plantuml()
    anlz.generate_json()
